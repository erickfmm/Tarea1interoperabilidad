
module.exports.getMessages = function (data){
    return [data.substring(1, data.length-3)];
    /*console.log(data);
    
    var startBlock = String.fromCharCode(11);
    var endBlock = String.fromCharCode(28);
    var carriageReturn = String.fromCharCode(13);

    var messages = data.match(/\u000b([^\u000b])/g);
    console.log(messages);
    for(var i in messages){
        messages[i] = messages[i].substring(1, messages[i].length-2);
    }
    
    return messages;*/
}

module.exports.GetDictionary = function (message){
    var messageDictionary = {};
    console.log("message: ");
    console.log(message);
    
    /*var fields = message.split('|');
    messageDictionary['PID'] = [[]]
    for(var ifield in fields){
        var subfields = fields[ifield].split('^');
        messageDictionary['PID'][ifield] = subfields;
    }*/

    var segments = message.split('\r');
    for(var segment of segments){
        var fields = segment.split('|');
        messageDictionary[fields[0]] = [[]]
        for(var ifield in fields){
            if(ifield == 0) continue;
            var subfields = fields[ifield].split('^');
            messageDictionary[fields[0]][ifield-1] = subfields;
        }
    }
    //console.log(messageDictionary);
    return messageDictionary;
}

module.exports.transformGender = function (messageDictionary){
    
    switch(messageDictionary['PID'][8-1][1-1].trim()){
        case 'M':
            messageDictionary['PID'][8-1][1-1] = '1';
            break;
        case 'F':
            messageDictionary['PID'][8-1][1-1] = '2';
            break;
    }
    //console.log(messageDictionary);
    return messageDictionary;
}

module.exports.GetStringMessage = function(messageDictionary){
    var messageString = '\u000b';
    for(var key of Object.keys(messageDictionary)){
        messageString += key;
        for(var ifield in messageDictionary[key]){
            messageString += '|'
            if(messageDictionary[key][ifield].length == 1){
                messageString += messageDictionary[key][ifield][0];
            }else {
                for(var subfield of messageDictionary[key][ifield]){
                    messageString += subfield+'^'
                }
                messageString = messageString.substring(0, messageString.length-1);
            }
            
        }
        messageString += '\n';
    }
    messageString += '\u001c\r';
    return messageString;
}

module.exports.getResponse = function (messageDictionary, timeStr){

    //var response = "MSH|^~\&|EnsembleHL7|ISC|HIS|HSJD|201703132131||ACK^A04|"+messageDictionary['MSH'][8][0]+"||\n\
    //MSA| AA|"+messageDictionary['MSH'][8][0];
    var responseDictionary = {};

    responseDictionary['MSH'] = [];
    responseDictionary['MSH'][0] = messageDictionary['MSH'][0] ;
    responseDictionary['MSH'][1] = ['EnsembleHL7'];
    responseDictionary['MSH'][2] = ['ISC'];
    responseDictionary['MSH'][3] = ['HIS'];
    responseDictionary['MSH'][4] = ['HSJD'];
    responseDictionary['MSH'][5] = [timeStr];
    responseDictionary['MSH'][6] = [''];
    responseDictionary['MSH'][7] = ['ACK', 'A04'];
    responseDictionary['MSH'][8] = messageDictionary['MSH'][8];

    responseDictionary['MSA'] = [];
    responseDictionary['MSA'][0] = ['AA'];
    responseDictionary['MSA'][1] = messageDictionary['MSH'][8];

    return responseDictionary;
}

module.exports.getFormatedDate = function(date){
    var month = date.getMonth()+1;
    month = ('00'+month).slice(-2);
    var year = ('0000'+date.getFullYear()).slice(-4);
    var day = ('00'+date.getDate()).slice(-2);
    var hour = ('00'+date.getHours()).slice(-2);;
    var minutes = ('00'+date.getMinutes()).slice(-2);;
    var seconds = ('00'+date.getSeconds()).slice(-2);;
    var strTime = year+month+day+hour+minutes+seconds;
    return strTime;
}

var mimensaje = "MSH|^~\&|HIS|HSJD|TAREA1|HSJD|20170313213108||ADT^A04^ADT_A01|31\n\
EVN|A04|20170313213108\n\
PID||1-9^^^Registro Civil^NI|||Soto^Juan^^Perez||19780416|M|||&&^^||\n\
PV1|1";

var miresponse = "MSH|^~\&|EnsembleHL7|ISC|HIS|HSJD|201703132131||ACK^A04| 31||\
MSA| AA| 31";

//var now = new Date();
//console.log(module.exports.GetStringMessage(module.exports.transformGender(module.exports.GetDictionary(mimensaje))));
//console.log(module.exports.GetStringMessage(module.exports.getResponse(module.exports.GetDictionary(mimensaje), module.exports.getFormatedDate(now))));